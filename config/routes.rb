Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post '/webhooks/telegram_vbc43edbf1614a075954dvd4bfab34l1' => 'webhooks#callback'

  post "post_message" , to: "admin#post_message"
  get "approve_dealer/:id" , to: "admin#approve_dealer"
  get "denied_dealer/:id" , to: "admin#denied_dealer"
  get "del_channel/:id" , to: "admin#del_channel"
  get "ban_dealer/:id" , to: "admin#ban_dealer"
  get "send_message" , to: "admin#send_message"
  get "photo" ,  to: "admin#get_photo"
  get "photo_url" ,  to: "admin#get_photo_url"
  get "users", to: "admin#users"
  get "shoka_channel", to: "admin#shoka_channel"
  root  'application#index'
  resources :channels
  end