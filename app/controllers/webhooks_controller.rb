class WebhooksController < ApplicationController
        skip_before_action :verify_authenticity_token
        require 'bot_command'
        require 'bot_message_dispatcher'
        require 'start'
        require 'end'
        require 'ask_for_tz'
        require 'ask_for_photo'
        require 'get_photo'
        require 'post_ad'
        require 'get_ad_text'
        require 'get_ad_photo'
        require 'channels'
        require 'edit_channels'
        require 'main_two'
        require 'main'

        def callback
          if webhook.nil? ||from.nil? || webhook[:channel_post] != nil
          head :ok
          return
          end
          dispatcher.new(webhook, user).process
          
          head :ok
       end

          def webhook
            return params['webhook']
          end
        
          def dispatcher
            BotMessageDispatcher
          end
        
       
          def from
         
           if webhook[:message] == nil    
            if webhook[:callback_query]!=nil && webhook[:callback_query][:from] !=nil
        
           
            return  webhook[:callback_query][:from]
            else
        
            end  
            if webhook[:edited_message] != nil
              return webhook[:edited_message][:from]
            end
            end

           
        
              if webhook[:message]!=nil && webhook[:message][:from] !=nil
        

                 
                 return   webhook[:message][:from]
              else
               if webhook[:edited_message] 
                return webhook[:edited_message][:from]
               end
              end
     
          
        end
       
        
    
        
          def user
            @user ||= User.find_by(telegram_id: from[:id]) || register_user
            @message = webhook[:message]
            @user 
          end
        
          
        
        
          def register_user
            @user = User.find_or_initialize_by(telegram_id: from[:id])
            @user.update_attributes!(first_name: from[:first_name], last_name: from[:last_name],user_name: from[:username])
            return @user
          end
end
