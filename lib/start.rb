module BotCommand
    class Start < Base
      def should_start?
       true
      end
  
      def start
       
        @user.reset_next_bot_command
        @user.set_next_bot_command('BotCommand::Ask_For_Tz')

        @button ='{
          "inline_keyboard": [
            [
        
              {
                "text": " 📝 הרשמה למערכת! 📝 ",
                "callback_data": "/Start"
              }
            ]
        
          ]
        }'


 send_message("📢 ברוך הבא,  #{@user.user_name}
הגעת לבוט ההרשמה למערכת של שוקה🎉
⚠️לצורך הרשמה יש למלא פרטים ולהמתין לאישור⚠️",@button)

      end

      def error
      end
      end
  end