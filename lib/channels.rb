module BotCommand
    class Channels < Base
      def should_start?
       true
      end
  
      def start

        @user.reset_next_bot_command
        @user.set_next_bot_command('BotCommand::EditChannels')

        if text =~ /\A\/Add/


                        @to_add = []
                        Channel.all.each do |c|
                            if !@user.channels.include?(c.id.to_s)
                                @to_add << c.id
                            end
                        end
    
         
                        @channels=''
                        @to_add.each do |i|
                            c = Channel.find(i)

                          @channels= @channels+   ' ,[
                        
                                {
                                "text": "'+c.name+'",
                                "callback_data": "/add_'+i.to_s+'"
                                }
                            ]'
                        
                        end


                        @button ='{
                            "inline_keyboard": [
                            [
                        
                            {
                                "text": "↪️ חזרה לתפריט הראשי",
                                "callback_data": "/Main"
                            }
                            ]
                            '+@channels+'
                        
                            ]
                        }'


                  if @to_add.count == 0
                    send_message("אין יותר ערוצים להוספה", @button)
                  else
                    send_message("אנא בחר ערוץ להוספה", @button)
                  end
    



        else

            
            @to_remove = []
            Channel.all.each do |c|
                if @user.channels.include?(c.id.to_s)
                    @to_remove << c.id.to_s
                end
            end


            @channels=''
            @to_remove.each do |i|
                c = Channel.find(i)
                @channels= @channels+    ' ,[
            
                    {
                    "text": "'+c.name+'",
                    "callback_data": "/remove_'+i+'"
                    }
                ]'
            
            end


            @button ='{
                "inline_keyboard": [
                [
            
                {
                    "text": "↪️ חזרה לתפריט הראשי",
                    "callback_data": "/Main"
                }
                ]
                '+@channels+'
            
                ]
            }'
      

            
            if @to_remove.count == 0
                send_message("אין יותר ערוצים להסרה", @button)
              else
                send_message("אנא בחר ערוץ להסרה", @button)
              end

        end

      end

      def error
      end
      end
  end