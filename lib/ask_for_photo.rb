module BotCommand
    class Ask_For_Photo < Base
      def should_start?
        if  text != nil
        @valid_one = text !="000000000"
        @valid_two = text =~ /[0-9]{9}/ 
        @valid_three = @user.is_valid_tz?(text)
        return @valid_one && @valid_two && @valid_three
        end
        false
      end
  
      def start
        @user.update_attributes!(tz:text)
         @user.reset_next_bot_command
         @user.set_next_bot_command('BotCommand::GetPhoto')
         send_message("
✅ מספר הזהות התקבל בהצלחה.
2️⃣ אנא שלח צילום תעודה מזהה / רישיון נהיגה.
⚠️ יש לשלוח צילום לא ערוך ובאיכות ברורה. ⚠️
                     ")
      end

      def error
         @getUserTzMessage = "
❌ מספר התעודת זהות אינו תקין, אנא נסה שנית! ❌
1️⃣  אנא הקש מספר תעודת זהות שלך כולל ספרת ביקורת.
⚠️ ( 9 ספרות ללא מקפים או רווחים ) ⚠️
        "

        send_message( @getUserTzMessage )
      end
    end
  end