module BotCommand
    class EditChannels < Base
      def should_start?
       text =~ /\A\/add/ ||text =~ /\A\/remove/
      end
  
      def start
        
            @user.reset_next_bot_command
            @user.set_next_bot_command('BotCommand::Main')
        

         @button ='{
            "inline_keyboard": [
            [
        
            {
                "text": "↪️ חזרה לתפריט הראשי",
                "callback_data": "/Main"
            }
            ]
           
        
            ]
         }'

        if text =~ /\A\/add/
            @id= text.delete("/add_")
            @user.channels << @id
            @user.save!
            send_message("✅ הערוץ נוסף בהצלחה", @button)
            return
        elsif text =~ /\A\/remove/
            @id= text.delete("/remove_")

            c = @user.channels.dup
            c.delete_at(c.index(@id))
            @user.channels = c
            @user.save!
            send_message("✅ הערוץ הוסר בהצלחה" , @button)
            return
        end
   
            

        @user.main
      end

      def error
        @user.main
      end
    end
end