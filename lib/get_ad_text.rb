module BotCommand
    class GetAdText < Base
      def should_start?
      text != nil && text.length < 201 
      end
  
      def start

        @ok = @user.post_ad(text)

      
       if @ok
        @time =Time.now
       end

       @user.update_attributes!(bot_command_data:{},post_time: @time)
       @user.set_next_bot_command('BotCommand::Main')

       @button ='{
        "inline_keyboard": [
          [
      
            {
              "text": "↪️ חזרה לתפריט הראשי",
              "callback_data": "/Start"
            }
          ]
      
        ]
      }'


       if @ok
       send_message('✅ המודעה פורסמה בהצלחה! ✅',@button)
      else
        send_message('❌ תקלה בשליחת ההודעה, אנא נסה שנית ❌',@button)
      end
      end

      def error
        send_message('❌ תקלה בשליחת ההודעה, אנא הזן עד כ- 200 תווים. ❌')
      end
      end
  end