module BotCommand
    class GetPhoto < Base
      def should_start?
       photo != nil
      end
  
      def start
   

        @user.update_attributes!(bot_command_data:{} , photo: photo)
        @user.reset_next_bot_command
        @user.set_next_bot_command('BotCommand::End')

        @button ='{
          "inline_keyboard": [
            [
        
              {
                "text": "📜 סיים הרשמה 📜",
                "callback_data": "/Start"
              }
            ]
        
          ]
        }'


        send_message('✅ התצלום התקבל בהצלחה.',@button)

      end

      def error
        send_message('
❌ התמונה לא התקבלה, אנא נסה שנית ❌
        ')
      end
      end
  end