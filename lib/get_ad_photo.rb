module BotCommand
    class GetAdPhoto < Base
      def should_start?
      photo != nil
      end
  
      def start
       @user.update_attributes!(post_photo:photo , bot_command_data:{})
       @user.set_next_bot_command('BotCommand::GetAdText')

    

       send_message('שים לב❗️
אנא שלח טקסט עבור המודעה, עד 200 תווים בלבד.')

      end

      def error
        send_message('❌ תקלה בשליחת התמונה, אנא הזן עד כ- 200 תווים. ❌')
      end
      end
  end