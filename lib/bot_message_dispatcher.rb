require 'telegram/bot'
class BotMessageDispatcher
    attr_reader :message, :user
  
    def initialize(message, user)
      @message = message
      @user = user

    end
  
    def process
      
      if @user.spam
        return
      end


      if @message[:message]!=nil && @message[:message][:chat] != nil &&  @message[:message][:chat][:id] !=nil && @message[:message][:chat][:id].to_s != user.telegram_id
##  הודעות שנשלחות מקבוצה לבוט , כרגע הבוט מתעלם מהן לגמרי
        return 
        end
      


      if user.get_next_bot_command
        bot_command = user.get_next_bot_command.safe_constantize.new(@user, @message)
        if bot_command.should_start?
          bot_command.start
        else
          bot_command.error
           end
      else
        start_command = BotCommand::Start.new(user, message)
        start_command.start
      end

    end
    
  end