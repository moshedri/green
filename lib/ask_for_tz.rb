module BotCommand
    class Ask_For_Tz < Base
      def should_start?
       true
      end
  
      def start
       
        @getUserTzMessage = "
1️⃣  אנא הקש מספר תעודת זהות שלך כולל ספרת ביקורת.
⚠️ ( 9 ספרות ללא מקפים או רווחים ) ⚠️
        "
         @user.reset_next_bot_command
         @user.set_next_bot_command('BotCommand::Ask_For_Photo')
         send_message( @getUserTzMessage )
      end
      def error
      end
    end
  end