require 'telegram/bot'

module BotCommand
  class Base
    attr_reader :user, :message, :api

    def initialize(user, message)
      @user = user
      @message = message
    end

    def should_start?
      raise NotImplementedError
    end

    def start
      raise NotImplementedError
    end
    def error
        raise NotImplementedError
      end
    protected

    def send_message(text, options={})
       @user.send_message(text,options)
    end

    
    def video
      if message[:message]!= nil &&  message[:message][:video] != nil
      return message[:message][:video][:file_id].to_s
      end
    end

    def photo
        if message[:message]!= nil &&  message[:message][:photo] != nil 
          int =message[:message][:photo].count
          int =int-1
               return message[:message][:photo][int][:file_id].to_s  
        end
    end

  
  
 

    def text

      if @message[:message] != nil && @message[:message][:text] != nil
         return @message[:message][:text]
      else
        if @message[:callback_query] != nil && @message[:callback_query][:data] != nil
        
            return @message[:callback_query][:data]
        end
      end
    end


   

  end
end