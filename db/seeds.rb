# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Channel.create(name:"קרית שמונה והסביבה" ,link:"@Shoka_Kiryat_Shmona")
Channel.create(name:"טבריה והסביבה" ,link:"@Shoka_Tviria")
Channel.create(name:"חיפה והסביבה" ,link:"@Shoka_Haifa")
Channel.create(name:"זכרון יעקב והסביבה" ,link:"@Shoka_Zichron_Yaakov")
Channel.create(name:"נתניה והסביבה" ,link:"@Shoka_Netanya")
Channel.create(name:"הרצליה והסביבה" ,link:"@Shoka_Herzliya")
Channel.create(name:"פתח תקווה והסביבה" ,link:"@Shoka_Petah_Tikva")
Channel.create(name:"תל אביב והסביבה" ,link:"@Shoka_Tel_Aviv")
Channel.create(name:"רמת גן בני ברק" ,link:"@Shoka_Ramat_Gan")
Channel.create(name:"חולון - בת ים" ,link:"@Shoka_Holon")
Channel.create(name:"ירושלים והסביבה" ,link:"@Shoka_Jeruslem")
Channel.create(name:"ראשון לציון והסביבה" ,link:"@Shoka_Rishon_Lezion")
Channel.create(name:"נס ציונה רחובות" ,link:"@Shoka_Ness_Ziona")
Channel.create(name:"אשדוד והסביבה" ,link:"@Shoka_Ashdod")
Channel.create(name:"באר שבע והסביבה" ,link:"@Shoka_Beer_Sheva")
Channel.create(name:"אילת" ,link:"@Shoka_Eilat")


