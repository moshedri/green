class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :telegram_id
      t.string :user_name
      t.string :first_name
      t.string :last_name
      t.jsonb :bot_command_data, default: {}


      t.string :tz
      t.string :photo
      t.boolean :spam , default: false

      t.timestamps
    end
    add_index :users, :telegram_id  , unique: true
    add_index :users ,:tz 
  end
end
