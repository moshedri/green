class AddChannelsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :channels, :text , array:true, default: []
  end
end
